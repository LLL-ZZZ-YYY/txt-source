——那麼，誰會殺了我呢？
在菲洛絲城門前，從聽到他那句話開始，羅佐心中便產生了一個確信的東西。那就是他確信路易斯一定是和自己一樣的。

身為率兵者，在敵兵面前自暴自棄，伸出頭顱向神叫喊著：殺死自己吧。

對了，他是一樣的。他和自己一樣，並不認為自己的生命有任何必要。然後，毫無疑問在心底討厭著什麼，憎恨著什麼。雖然不知道對方的那個對象是誰，但對方明顯一直對其抱有難以掩飾的情感。

正因為如此，羅佐才會這麼想，路易斯的本質既不是惡德也不是大惡，而是自己的同類，是自己的燦爛——仇敵。
“你不覺得這個世界很愚蠢嗎？我的仇敵和同類喲。”

羅佐一邊張開看起來快要燃燒殆盡的嘴唇，一邊說。語氣十分淡然，很好地發揮著自己的口才。

在這個世界上，不正確的、正確的任何人，都在心底某處憎恨著別人。今天吃不到麵包，沒有可安眠的床鋪，父母在眼前被殺，女兒被士兵玩弄，在戰場上戀人的屍體被踐踏。不幸和憎惡的種子到處都在播撒著。

雖然這麼說，但人們卻裝作一副和憎惡無緣的樣子，認為這樣做是對的。
儘管如此，只要說出口一次，他們就會像瘋狂一樣繼續傾吐自己的感情。就算是菲洛絲的市民也是這樣。

讓自己焦慮的她，菲洛絲·托雷特是正確的人。她思念市民，愛市民，有時甚至不惜讓自己扮演被憎恨的角色。她作為統治者，是比任何人都正確、都相稱的人。
然後這樣的她怎麼樣了呢？一旦被掛上了背徳者的牌子，市民們就很開心地把石頭扔向了她，用棒子打向她的身體。庇護她的人不過屈指可數而已。

看不下去。結果那些傢伙們壓根沒有思考，只是成為被憎惡所驅使的傀儡而已，以此過著每一天的生活。這真是傑作，簡直就完全是喜劇演員一樣。
所以羅佐才會這麼想，給他們一個適合的下場。正確的她，在這個世上也只有憎惡和惡意這樣的真實存在。這才是正確的姿態。

羅佐繼續著講述，露出了自嘲般的笑容。嘴唇在火焰中波動。
“這麼說來，我可不能否定憎惡。我寧願肯定這個世上所有的憎惡。”
正因為如此，才應該將這一切都燃燒殆盡。那些否定憎惡的人們，將憎惡當作毫無意義的東西來對待，他們都述說著自己是正確的。
我要把這些傢伙全部都打敗，讓他們燃起心中寄宿的憎惡。在這些沸騰的同時，用正確的憎惡來填滿這個世界吧。
因為羅佐相信，這才是此世的正確姿態。可以說，燃燒憎惡才是自己的根源、原典。
“同類。你也一樣吧。至此，讓你的雙腳奔跑起來的燃料，無疑就是憎惡。”
羅佐看著一邊在眼前喘氣，一邊朝著睜開眼睛的路易斯高聲說道。路易斯的視線貫穿了羅佐。


◇◆◇◆
憎惡，憎惡？在心中，輕輕地嘟噥著。我一邊傾聽著羅佐的話語，一邊在眼中浮現出一個情景。

那是對曾經經歷的旅途的記憶，是曾經見過的那個底層。在那個的盡頭，填滿我心深處，推動我身體前行的東西，到底是什麼呢？
事到如今，用不著再提問了，那是像羅佐說的那樣，讓人無法反駁的憎惡。真是任性，無論到哪裡都是粗暴的想法。事到如今能否定那個嗎？

太陽般的英雄，擁有我所不擁有的全部，騎士團的俊英，我簡直無法用語言形容其強大。魔法師殿下，精靈公主，還有阿琉爾娜。她們光輝奪目，甚至連我的指尖都無法觸及。

啊，雖然很討厭，但是很羨慕。

也有被踐踏了的事，也有尊嚴被踢飛了的事。僅僅靠意志是無法到達的存在，對此我多少次咬牙切齒。不知受過多少次屈辱，沒有被給予救贖，也沒有被給予絲毫敬意的那些日子。那是光是想起來就覺得噁心的日常生活。

如果這麼想，確實我和羅佐是同類吧。一定是那樣吧。如果說那傢伙每天和我過著同樣的生活，應該也會與憎惡和羨慕為伍。羅佐的話語中，確實有讓人如此聯想的部分。
心臟像燒焦了一樣熱。路易斯扭曲著嘴唇，抬起臉頰說。
“羅佐，我呢，我不能否定你的話。我的確曾經習慣憎惡，也不知道多少次心生羨慕。從這個意義上來說，我和你是同類。”
不管怎麼掙扎，我都無法否定心中的憎惡。我想，今後我也會一直將這黑暗的東西保留在心底，以此生存下去。

但是，可是。
路易斯歪著嘴，繼續著語言。
“——但是，即便如此，也不是同種啊。不同的只有一個，你選擇就此燒盡一切，而我則對這樣卑微的我感到焦急。僅此而已。”
粗暴的呼吸從口中流出。通過喉嚨的呼氣本身似乎燒灼著呼吸道。路易斯用左手強行握緊寶劍，無視太陽穴裡盤旋的遲鈍的疼痛。在遠處，能看到有什麼東西在慢慢地移動。

唯獨這一點，實在難以容忍。我並不想把自己憧憬的英雄們就這樣以憎恨的心情燒掉。
“我啊，不是想打飛他們，也不是想貶低他們——我只想，和他們並肩站到一起。”
啊啊，有什麼奇怪的東西從心裡涌上來了。
我想向那些光輝的英雄們伸出手去。我不僅想追逐他們的背影，還想成為能夠與他們共同前進的存在。我的心底深處，只有那耀眼的憧憬。為了做到這些，甚至捨棄生命也在所不惜。
正因為如此，說出來吧。我和羅佐是同類，但是，不是同種。
“……真遺憾。那麼，我要把你燒成灰燼，怨敵。”
羅佐回應的話，簡直就像是從心底發出的聲音一樣。其大幅扭曲的眼睛所映出的情感，簡直讓人聯想起悲傷。真的，那是一副無論到哪裡看起來都是悲傷的表情。
為了回應羅佐，路易斯只用左手握著寶劍，將刀刃放在右肩上。

身體已經像烤肉一樣，皮膚發出燒焦了一樣的扭曲的聲音。自己有被從內部蒸熟的感覺。但是不可思議的是，在心底深處還有別的熱度，不是燒灼的熱度，而是相當舒適的熱量。
眼睛彷如在燃燒，我看見有影子在動。
“放心吧，放心吧。我就在這裡拯救你。”
我和羅佐，我想一定在根本的地方有同樣的經歷和感情吧。但是，不知道為什麼，我和他卻走上了歧路。

理由什麼的根本就不知道。我不知道他的過去，那傢伙也不知道我的過去。一定彼此連互相了解都不想去做。說不定只是某個很小的事物上產生了分歧，可能就只是那樣的理由。
但是，如果硬要說的話，我有青梅竹馬的阿琉爾娜，又有南因絲養母，還有老爺子。然後，他沒有。一定只是那麼小的差別。

因為這些細微的差別，那傢伙終於連自己的憧憬都燃燒殆盡了。內心深處，有著無法言喻的思念。
讓寶劍在肩膀上鳴響，踏著紅磚，路易斯再次從羅佐的身體中看到了火星在燃燒。火蛇們像有著敵意一樣看向這邊。

那一瞬間，路易斯閉上雙眼，向寶劍許願。
——沒有必要祈求。你的祈求我早已知曉。如果是主人的追求，那就為這一擊而盡力吧。因為我是為此而存在的道具。
那樣的聲音，在腦海中響起的同時，路易斯開始行動，以要倒下一樣的氣勢，把身體托付給寶劍。

羅佐的炎熱，在眼前閃耀般地搖晃著。那雙眼睛依舊燃著炯炯的火焰，灼燒著夜晚。