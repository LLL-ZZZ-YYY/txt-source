葬禮結束後緊接而來的是晚餐會。
大家在談笑間享用美食與佳釀。
就這點來說，庶民和王族的葬禮沒什麼不同，就差在食物美味太多。

「我深表遺憾。」

一名臃腫的中年男子，用尖銳的聲音向我搭話。這人是列支敦王国的約阿希姆陛下。

「約阿希姆陛下，感謝你前來吊問。」
「哦哦，你記得我的名字啊，那個⋯⋯」「我是維塞，齊默爾曼，海德堡。還請你多多指教。」
「初次見面，維塞陛下。」

一名爽朗的年輕国王問候我。

「你是腓特烈陛下吧，非常榮幸見到你。」

我国是被諸大国包圍的小小国家。海德堡王国的西側是列支敦王国，東側是明克斯王国，兩個都是大国。
若是能從天空俯望我国的話，看起來大概就像是荷包蛋的蛋黃。
而我国，就是靠調整兩大国的国力來維持和平。譬如列支敦王国若發生水患，我国將無償提供麥子並借款援助。明克斯王国打算增加軍備時，則催促他們償還債務削減国力。
因此必須和兩国国王打好關係才行。
不過這個約阿希姆王，竟然抓著羅絲公主的手摸來摸去。

「蘿絲公主，請妳不要再哭泣了。妳這模樣就好像是被雨打濕的玫瑰一樣令人心疼。我有什麼能幫上妳的嗎？」

這該死的大叔，我妹都露出看雜碎的眼神了，你還不知道她是在嫌棄你嗎？她之所以會哭，純粹是因為你太噁心了。

「約阿希姆陛下，請用我国自豪的紅酒。很美味喔。」

我往這中年大叔的酒杯裡倒酒，紅酒裡還特地多加了點伏特加。

「謝謝。」

我也飲下杯中物，只不過我的是葡萄汁。
雖然我愛喝酒，不過身為一国之君總不能在儀式場合喝個爛醉。

「嗝。」
「真是好酒量，再來一杯吧。」

我向蘿絲公主使了個眼色，要她回自己房間。妹妹點了點頭，便用手帕遮住嘴巴離席了。
場子漸漸熱絡起來，所有人各自享受了這場餐會，這次到底是祖父的葬禮，妹妹不在場應該也沒關係吧。